package com.medium.cordova.parse.push;

import android.content.Context;
import com.google.android.gcm.GCMBroadcastReceiver;

public class GCMReceiver extends GCMBroadcastReceiver
{

   @Override
   protected String getGCMIntentServiceClassName(Context context)
   {
      return "com.medium.cordova.parse.push.GCMIntentService";
   }
}