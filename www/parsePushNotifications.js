var exec = require('cordova/exec');
var pluginNativeName = "ParsePushNotificationPlugin";

var ParsePushPlugin = function() {
};

ParsePushPlugin.prototype = {
  register: function(options, successCallback, errorCallback) {
    exec(successCallback, errorCallback, pluginNativeName, 'register', [options]);
  }
};

module.exports = new ParsePushPlugin();
