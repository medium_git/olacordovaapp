package com.medium.cordova.parse.push;

import com.google.android.gcm.GCMBaseIntentService;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

@SuppressLint("NewApi")
public class GCMIntentService extends GCMBaseIntentService
{

   private static final String TAG = "GCMIntentService";

   public GCMIntentService()
   {
      super("GCMIntentService");
   }

   @Override
   public void onRegistered(Context context, String regId)
   {
      // do nothing - the parse receiver takes care of registrations
   }

   @Override
   public void onUnregistered(Context context, String regId)
   {
      // do nothing - the parse receiver takes care of registrations
   }

   @Override
   // method is called when incoming message is received (NOT opened)
   protected void onMessage(Context context, Intent intent)
   {
      Log.d(TAG, "onMessage - context: " + context);

      if (ParsePushNotificationPlugin.isInForeground())
      {
         // TODO: Jeff, do not alert notification if we are still in isInForeground
      }
   }

   @Override
   public void onError(Context context, String errorId)
   {
      Log.e(TAG, "onError - errorId: " + errorId);
   }

}
