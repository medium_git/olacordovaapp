package com.medium;

import com.crashlytics.android.Crashlytics;
import android.app.Application;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.medium.hyperlocal.R;

import io.fabric.sdk.android.Fabric;

public class OlaApplication extends Application
{

   public OlaApplication()
   {
      super();
   }

   public void onCreate()
   {
      super.onCreate();
      Fabric.with(this, new Crashlytics());
      Parse.initialize(getApplicationContext(), getString(R.string.parse_app_id), getString(R.string.parse_client_key));
      ParseInstallation.getCurrentInstallation().saveInBackground();
   }
}
