package com.medium.cordova.parse.push;

import java.util.ArrayList;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

public class ParsePushNotificationPlugin extends CordovaPlugin
{
   public static final String TAG = "ParsePushNotificationPlugin";

   private static CordovaWebView gWebView;

   private static boolean isActive = false;
   private static ArrayList<String> callbackQueue = new ArrayList<String>();

   /**
    * Gets the application context from cordova's main activity.
    * 
    * @return the application context
    */
   private Context getApplicationContext()
   {
      return this.cordova.getActivity().getApplicationContext();
   }

   @Override
   public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException
   {
      Log.v(TAG, "execute: action=" + action);

      if (action.equalsIgnoreCase("register"))
      {
         JSONObject params = args.optJSONObject(0);

         // login user and link Installation with User
         String username = params.optString("username", "");
         String password = params.optString("password", "");

         try
         {
            ParseUser user = ParseUser.logIn(username, password);
            ParseInstallation.getCurrentInstallation().put("user", user);
            ParseInstallation.getCurrentInstallation().saveInBackground();
            callbackContext.success();

         }
         catch (ParseException e)
         {
            Log.v(TAG, "login failed with username;" + username + "; Installation object not created");
         }

         return true;
      }
      return false;
   }

   public static void onNotificationReceived(String data)
   {
      // only send JS command if APP is in foreground, or else wait for Cordova onPageFinished event to trigger JS cmd
      if (isActive)
      {
         String js = "javascript:setTimeout(function(){window.plugin.parse_push.ontrigger(" + data + ");},0)";
         gWebView.sendJavascript(js);
      }
      else
      {
         String js = "javascript:setTimeout(function(){global.initFromPush = " + data + ";},0)";
         callbackQueue.add(js);
      }
   }

   @Override
   public void initialize(CordovaInterface cordova, CordovaWebView webView)
   {
      super.initialize(cordova, webView);
      gWebView = webView;
      isActive = true;
   }

   @Override
   public void onDestroy()
   {
      super.onDestroy();
      gWebView = null;
      isActive = false;
   }

   @Override
   public void onPause(boolean multitasking)
   {
      super.onPause(multitasking);
   }

   @Override
   public void onResume(boolean multitasking)
   {
      super.onResume(multitasking);
      isActive = true;
   }

   @Override
   public Object onMessage(String id, Object data)
   {
      if (id.equals("onPageFinished"))
      {
         cordova.getThreadPool().execute(new Runnable()
         {
            @Override
            public void run()
            {
               flushCallbackQueue();
            }
         });
      }
      return null;
   }

   private void flushCallbackQueue()
   {
      for (String js : callbackQueue)
      {
         if (gWebView != null)
            gWebView.sendJavascript(js);
      }

      callbackQueue.clear();
   }

   public static boolean isActive()
   {
      return gWebView != null;
   }

   public static boolean isInForeground()
   {
      return isActive;
   }

}
