package com.medium.cordova.parse.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.medium.hyperlocal.MainActivity;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jeffreychang on 2/23/15.
 */
public class OlaPushBroadcastReceiver extends ParsePushBroadcastReceiver
{

   @Override
   protected void onPushReceive(Context context, Intent intent) {
      JSONObject pushData = null;

      String title = "", alert = "";
      try {
         pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
         title = pushData.getString("title");
         alert = pushData.getString("alert");
      } catch (JSONException var7) {
         android.util.Log.e("ParsePushReceiver", "Unexpected JSONException when receiving push data: ", var7);
      }

      String action = null;
      if(pushData != null) {
         action = pushData.optString("action", (String)null);
      }

      if(action != null) {
         Bundle notification = intent.getExtras();
         Intent broadcastIntent = new Intent();
         broadcastIntent.putExtras(notification);
         broadcastIntent.setAction(action);
         broadcastIntent.setPackage(context.getPackageName());
         context.sendBroadcast(broadcastIntent);
      }

      Notification notification = this.getNotification(context, intent);
      if(context != null && notification != null) {
         NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
         int notificationId = 0;
         notificationId = String.format("title:%s, message: %s", title, alert).hashCode();

         try {
            nm.notify(notificationId, notification);
         } catch (SecurityException var6) {
            notification.defaults = 5;
            nm.notify(notificationId, notification);
         }
      }
   }

   @Override
   protected void onPushOpen(Context context, Intent intent)
   {
      // launch or bring CordovaApp Activity to foreground
      Intent i = new Intent(context, MainActivity.class);
      i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(i);

      processPushBundle(intent);
   }

   private void processPushBundle(Intent intent)
   {
      Bundle extras = intent.getExtras();
      if (extras != null)
      {
         ParsePushNotificationPlugin.onNotificationReceived(extras.getString("com.parse.Data"));
      }
   }
}
